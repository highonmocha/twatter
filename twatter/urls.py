from django.conf.urls import patterns, include, url
from django.contrib import admin

from registration.views import home

from django.conf import settings

admin.autodiscover()
urlpatterns = [
	url(r'^$', home, name='home'),
	url(r'^registration/', include('registration.urls', namespace='registration')),
	url(r'^admin/', include(admin.site.urls)),
]

# while in development, it is okay for django to serve media by itself
# in production, this should be done by dedicated webserver (apache / nginx)
if settings.DEBUG:
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
		'document_root': settings.MEDIA_ROOT}))
