from django.conf.urls import url

from registration import views

urlpatterns = [
	url(r'^signup/$', views.signup, name='signup'),
	url(r'^profile/$', views.profile, name='profile'),
	url(r'^welcome/$', views.welcome, name='welcome'),
	url(r'^login/$', views.login, name='login'),
	url(r'^logout/$', views.logout, name='logout'),
	url(r'^edit/$', views.edit, name='edit'),
]