from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from registration.models import Twat, UserProfile
from django.utils import timezone
from django.core.files import File

class SignupForm(forms.Form):
	username = forms.RegexField(regex=r'^\w+$',
								max_length=30,
								widget=forms.TextInput(attrs={'class': 'form-control'}),
								label=_(u'username'),
								error_messages={
									'required': 'Please enter a username.', 
									'invalid': "Your username may only contain letters and numbers."
								})

	email = forms.EmailField(widget=forms.TextInput(attrs={'maxlength': 75, 'class': 'form-control'}),
							 label=_(u'email'),
							 required=True,
							 error_messages={'required': 'Please enter an email.'})

	password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
							   label=_(u'password'),
							   required=False)

	def clean(self):
		username = self.cleaned_data.get('username', '')
		password = self.cleaned_data.get('password', '')
		email = self.cleaned_data.get('email', None)
		if email:
			email_exists = User.objects.filter(email__iexact=email).count()
			if email_exists:
				raise forms.ValidationError(_(u'Someone is already using that email address.'))
		exists = User.objects.filter(username__iexact=username).count()
		if exists:
			user_auth = authenticate(username=username, password=password)
			if not user_auth:
				raise forms.ValidationError(_(u'Someone is already using that username.'))
		return self.cleaned_data

	def save(self, profile_callback=None):
		username = self.cleaned_data['username']
		password = self.cleaned_data['password']

		email = self.cleaned_data.get('email', None)
		if email:
			email_exists = User.objects.filter(email__iexact=email).count()
			if email_exists:
				raise forms.ValidationError(_(u'Someone is already using that email address.'))

		exists = User.objects.filter(username__iexact=username).count()
		if exists:
			user_auth = authenticate(username=username, password=password)
			if not user_auth:
				raise forms.ValidationError(_(u'Someone is already using that username.'))
			else:
				return user_auth
		
		# kept here for convenience for now, should be changed to prompt
		# the user to create a password if he wants to login.
		if not password:
			password = username
			
		new_user = User(username=username)
		new_user.set_password(password)
		new_user.is_active = False
		new_user.email = email
		new_user.save()
		new_user = authenticate(username=username,
								password=password)
		# create relation to UserProfile
		new_userprofile = UserProfile(user=new_user)
		new_userprofile.about = "Bio"
		new_userprofile.save()
		return new_user

class LoginForm(forms.Form):
	username = forms.RegexField(regex=r'^\w+$',
								max_length=30,
								required=True,
								label=_(u'username'),
								widget=forms.TextInput(attrs={'class': 'form-control'}))

	password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}),
								label=_(u'password'),
							   required=True)

	def __init__(self, *args, **kwargs):
		self.user_cache = None
		super(LoginForm, self).__init__(*args, **kwargs)

	def clean(self):
		username = self.cleaned_data.get('username', '')
		password = self.cleaned_data.get('password', '')
		if username is not '' and password is not '':
			exists = User.objects.filter(username__iexact=username).count()
			if exists:
				self.user_cache = authenticate(username=username,
										 password=password)
				if not self.user_cache:
					raise forms.ValidationError(_(u'Invalid username or password.'))
			else:
				raise forms.ValidationError(_(u'This username does not exist.'))
		return self.cleaned_data

class TwatForm(forms.Form):
	tweet = forms.CharField(widget=forms.Textarea(attrs={'maxlength': 140, 'class': 'form-control'}),
							 label=_(u'tweet'),
							 required=False,
							 error_messages={'max_length': 'Maximum length is 140 characters.'})

	def __init__(self, user=None, *args, **kwargs):
		super(TwatForm, self).__init__(*args, **kwargs)
		self.author = User.objects.filter(username__iexact=user.username)[0]

	def save(self):
		tweet = self.cleaned_data.get('tweet')
		pub_tweet = self.author.twat_set.create(text=tweet, pub_date=timezone.now())
		return pub_tweet

class ProfileEdit(forms.Form):
	about = forms.CharField(label=_(u'About me'),
							widget=forms.Textarea(attrs={'maxlength':200, 'class':'form-control'}),
							error_messages={'max_length': 'Maximum length is 200 characters.'})

	pic = forms.ImageField(label=_(u'Display pic'),
						   error_messages={'invalid_image':'Thats an invalid image.'})

	def __init__(self, user=None, *args, **kwargs):
		super(ProfileEdit, self).__init__(*args, **kwargs)
		self.user = User.objects.filter(username__iexact=user.username)[0]

	def save(self):
		new_pic = self.cleaned_data['pic']
		new_about = self.cleaned_data['about']
		self.user.userprofile.about = new_about
		self.user.userprofile.display_pic = new_pic
		self.user.userprofile.save()