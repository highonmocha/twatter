from django.db import models
from django.contrib.auth.models import User
from django import forms
from django.db import models

class UserProfile(models.Model):
	user = models.OneToOneField(User)
	display_pic = models.ImageField(upload_to='dp/', default='dp/default.png')
	about = models.TextField(max_length=200)

	def __unicode__(self):
		return self.user.username

class Twat(models.Model):
	author = models.ForeignKey(User)
	text = models.TextField(max_length=140)
	pub_date = models.DateTimeField('date published')

	def __unicode__(self):
		return self.text