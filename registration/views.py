from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import login as login_user
from django.contrib.auth import authenticate
from django.contrib.auth import logout as logout_user
from registration.models import Twat, UserProfile
from registration.forms import SignupForm, LoginForm, TwatForm, ProfileEdit

# Create your views here.
def signup(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/registration/profile')
	if request.method == 'POST':
		form = SignupForm(request.POST)
		if form.is_valid():
			new_user = form.save()
			login_user(request, new_user)
			return HttpResponseRedirect('/registration/profile/')
	else:
		form = SignupForm()
	return render(request, 'signup.html', {
		'form' : form,
	})

def login(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/registration/profile/')
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			user = form.user_cache
			if user.is_authenticated():
				login_user(request, user)
				return HttpResponseRedirect('/registration/profile/')
	else:
		form = LoginForm()
	return render(request, 'login.html', {
		'form' : form,
	})

def profile(request):
	if request.user.is_authenticated():
		if request.method == 'POST':
			form = TwatForm(user=request.user, data=request.POST)
			if form.is_valid():
				form.save()
		else:
			form = TwatForm(user=request.user)
		all_twats = Twat.objects.all().order_by('-pub_date')
		return render(request, 'profile.html', {
			'form' 	: form,
			'twats' : all_twats
		})
	else:
		return HttpResponseRedirect('/registration/login/')

def welcome(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/registration/profile/')
	else:
		return render(request, 'welcome.html')

def logout(request):
	logout_user(request)
	return HttpResponseRedirect('/registration/login/')


def home(request):
	return HttpResponseRedirect('/registration/welcome/')

def edit(request):
	if request.user.is_authenticated():
		if request.method == 'POST':
			form = ProfileEdit(user=request.user, data=request.POST, files=request.FILES)
			if form.is_valid():
				form.save()
				return HttpResponseRedirect('/registration/profile/')
		else:
			form = ProfileEdit(user=request.user)
			pic = UserProfile.objects.filter(user=request.user).get().display_pic
			about = UserProfile.objects.filter(user=request.user).get().about
		return render(request, 'edit_profile.html', {
			'form': form,
			'about': about,
			'pic': pic
		})
	else:
		return HttpResponseRedirect('/registration/welcome/')